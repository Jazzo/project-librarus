package service;

import dto.BorrowerDto;

import java.util.List;

public interface BorrowerService {
    List<BorrowerDto> findAll();
}
