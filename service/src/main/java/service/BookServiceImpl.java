package service;

import dto.BookDto;
import model.*;
import repo.AuthorRepository;
import repo.AuthorRepositoryImpl;
import repo.BookRepository;
import repo.BookRepositoryImpl;
import lombok.extern.slf4j.Slf4j;

import java.util.*;
import java.util.stream.Collectors;

@Slf4j
public class BookServiceImpl implements BookService {

    private final BookRepository bookRepository;
    private final AuthorRepository authorRepository;

    public BookServiceImpl(){
        authorRepository = new AuthorRepositoryImpl();
        bookRepository = new BookRepositoryImpl();
    }

    @Override
    public void add(BookDto bookDto) {
        Optional<Author> author = Optional.ofNullable(authorRepository.find(bookDto.getAuthorId()));

        author.ifPresent(a->{
            Book book = setUpBook(bookDto, new Book(), a);
            bookRepository.save(book);
        });
    }

    @Override
    public void edit(BookDto bookDto) {
        Book currentBook = Optional.ofNullable(bookRepository.find(bookDto.getId()))
                .orElseThrow(() -> {
                    log.error("[]Can not find book] Id: {}", bookDto.getId());
                    return new IllegalArgumentException("Can not find book");
                });
        Author author = Optional.ofNullable(authorRepository.find(bookDto.getAuthorId()))
                .orElse(currentBook.getAuthor());

        Book book = setUpBook(bookDto, currentBook, author);
        bookRepository.edit(book);
    }

    @Override
    public void delete(Long bookId) {
        bookRepository.delete(bookId);
        log.info("[Book has been deleted] Id: {}", bookId);
    }

    @Override
    public BookDto find(Long bookId) {
        Book book = bookRepository.find(bookId);
        if (Objects.nonNull(book)){
            String borrowerName = findCurrentBorrower(book);
            Long borrowId = findCurrentBorrowerId(book);
            return new BookDto(book.getId(), book.getTitle(), book.getRelease(), book.getIsbn(), book.getAuthor().getDisplayName(),
                    book.getCategory(), book.getPages(), book.isBorrow(), borrowerName, book.getSummary(), book.getAuthor().getId(), borrowId);
        }
        throw new IllegalArgumentException("Can not find book");
    }

    @Override
    public List<BookDto> findAll() {
        List<Book> books = bookRepository.findAll();

        return books.stream()
                .map(book -> {
                    String borrowerName = findCurrentBorrower(book);
                    Long borrowId = findCurrentBorrowerId(book);
                    return new BookDto(book.getId(), book.getTitle(), book.getRelease(), book.getIsbn(), book.getAuthor().getDisplayName(),
                            book.getCategory(), book.getPages(), book.isBorrow(), borrowerName, book.getSummary(), book.getAuthor().getId(), borrowId);
                }).collect(Collectors.toList());
    }

    @Override
    public List<String> findAllCategories() {
        return Arrays.stream(BooksType.values())
                .map(Enum::name)
                .collect(Collectors.toList());
    }

    private String findCurrentBorrower(Book book) {
        String borrowerName = null;
        if (book.isBorrow()) {
            Optional<Borrower> borrower = book.getBorrows().stream()
                    .max(Comparator.comparing(Borrow::getId))
                    .map(Borrow::getBorrower);
            if (borrower.isPresent()) {
                borrowerName = borrower.get().getDisplayName();
            }
        }
        return borrowerName;
    }

    private Long findCurrentBorrowerId(Book book) {
        Long borrowId = null;
        if (book.isBorrow()) {
            Optional<Long> currentBorrowId = book.getBorrows().stream()
                    .max(Comparator.comparing(Borrow::getId))
                    .map(Borrow::getId);
            if (currentBorrowId.isPresent()) {
                borrowId = currentBorrowId.get();
            }
        }
        return borrowId;
    }
    private Book setUpBook(BookDto bookDto, Book book, Author author){
        book.setAuthor(author);
        book.setTitle(bookDto.getTitle());
        book.setRelease(bookDto.getRelease());
        book.setPages(bookDto.getPages());
        book.setIsbn(bookDto.getIsbn());
        book.setSummary(bookDto.getSummary());
        book.setCategory(bookDto.getCategory());
        return book;
    }
}
