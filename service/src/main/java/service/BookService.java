package service;

import dto.BookDto;

import java.util.List;

public interface BookService {

    void add(BookDto bookDto);

    void edit(BookDto bookDto);

    void delete(Long bookId);

    BookDto find(Long bookId);

    List<BookDto> findAll();

    List<String> findAllCategories();
}
