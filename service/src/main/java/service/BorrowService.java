package service;

import dto.BorrowDto;

public interface BorrowService {
    void save(BorrowDto borrowDto);

    void delete(Long id);
}
