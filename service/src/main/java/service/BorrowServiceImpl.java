package service;

import dto.BorrowDto;
import lombok.extern.slf4j.Slf4j;
import model.Book;
import model.Borrow;
import model.Borrower;
import repo.*;

import java.time.LocalDate;
import java.util.Optional;

@Slf4j
public class BorrowServiceImpl implements BorrowService {

    private final BorrowRepositoryImpl borrowRepository;
    private final BookRepositoryImpl bookRepository;
    private final BorrowerRepositoryImpl borrowerRepository;

    public BorrowServiceImpl(BorrowRepository borrowRepository, BookRepository bookRepository, BorrowerRepository borrowerRepository) {
        this.borrowRepository = new BorrowRepositoryImpl();
        this.bookRepository =  new BookRepositoryImpl();
        this.borrowerRepository = new BorrowerRepositoryImpl();
    }

    @Override
    public void save(BorrowDto borrowDto) {
        Long borrowerId = borrowDto.getBorrowerId();
        Long bookId = borrowDto.getBookId();

        Borrower borrower = Optional.ofNullable(borrowerRepository.find(borrowerId))
                .orElseThrow(() -> {
                    log.error("[Can not find borrower] Id: {}", borrowerId);
                    return new IllegalArgumentException("Can not find borrower");
                });

        Book book = Optional.ofNullable(bookRepository.find(bookId))
                .orElseThrow(() -> {
                    log.error("[Can not find item] Id: {}", bookId);
                    return new IllegalArgumentException("Can not find item");
                });

        if (book.isBorrow()) {
            log.error("[Item has already been borrowed] Id: {}", bookId);
            throw new IllegalArgumentException("Item has already been borrowed");
        }

        Borrow borrow = setUpBorrow(borrower, book);
        borrowRepository.save(borrow);

        book.setBorrow(true);
        bookRepository.edit(book);
    }

    @Override
    public void delete(Long id) {
        Borrow borrow = Optional.ofNullable(borrowRepository.find(id))
                .orElseThrow(() -> {
                    log.error("[]Can not find borrower] Id: {}", id);
                    return new IllegalArgumentException("Can not find borrow");
                });

        Book book = borrow.getBook();
        book.setBorrow(false);
        bookRepository.edit(book);

        borrowRepository.delete(borrow);
        log.info("[Borrow has been deleted] Id: {}", id);
    }

    private Borrow setUpBorrow(Borrower borrower, Book book) {
        Borrow borrow = new Borrow();
        borrow.setBorrower(borrower);
        borrow.setBook(book);
        borrow.setRentalDate(LocalDate.now());
        return borrow;
    }

    }
