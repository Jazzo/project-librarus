package service;

import dto.AuthorDto;
import model.Author;
import repo.AuthorRepository;
import repo.AuthorRepositoryImpl;

import java.util.List;

public class AuthorServiceImpl implements AuthorService {

    private final AuthorRepository authorRepository;


    public AuthorServiceImpl() {
        authorRepository = new AuthorRepositoryImpl();
    }

    @Override
    public void saveAuthor(Author author) {authorRepository.save(author);
    }

    @Override
    public Long editAuthor(Author author) throws Exception {
        return null;
    }

    @Override
    public List<AuthorDto> findAllAuthors() {
        return null;
    }
}