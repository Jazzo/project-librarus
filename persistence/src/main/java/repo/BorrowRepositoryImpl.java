package repo;

import model.Borrow;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import util.HibernateUtil;

public class BorrowRepositoryImpl implements BorrowRepository {
    @Override
    public void save(Borrow borrow) {
        Transaction transaction = null;
        try (Session session = HibernateUtil.openSession()) {
            transaction = session.getTransaction();
            transaction.begin();
            session.persist(borrow);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
        }
    }

    @Override
    public Borrow find(Long borrowId) {
        Transaction transaction = null;
        Borrow borrow = null;
        try (Session session = HibernateUtil.openSession()) {
            transaction = session.getTransaction();
            transaction.begin();
            Query query = session.createQuery("from Borrow b where b.id = :id")
                    .setParameter("id", borrowId);
            borrow = (Borrow) query.uniqueResult();
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
        }
        return borrow;
    }

    @Override
    public void delete(Borrow borrow) {
        Transaction transaction = null;
        try (Session session = HibernateUtil.openSession()) {
            transaction = session.getTransaction();
            transaction.begin();
            session.delete(borrow);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
        }
    }
    }
