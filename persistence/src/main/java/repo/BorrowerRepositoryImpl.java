package repo;

import model.Borrower;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import util.HibernateUtil;

import java.util.ArrayList;
import java.util.List;

public class BorrowerRepositoryImpl implements BorrowerRepository {
    @Override
    public List<Borrower> findAll() {
        List<Borrower> borrower = new ArrayList<>();
        Transaction transaction = null;
        try (Session session = HibernateUtil.openSession()) {
            transaction = session.getTransaction();
            transaction.begin();
            borrower = session.createQuery("from Borrower").list();
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
        }
        return borrower;
    }

    @Override
    public Borrower find(Long borrowerId) {
        Transaction transaction = null;
        Borrower borrower = null;
        try (Session session = HibernateUtil.openSession()) {
            transaction = session.getTransaction();
            transaction.begin();
            Query query = session.createQuery("from Borrower b where b.id = :id")
                    .setParameter("id", borrowerId);
            borrower = (Borrower) query.uniqueResult();
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
        }
        return borrower;
    }
}
