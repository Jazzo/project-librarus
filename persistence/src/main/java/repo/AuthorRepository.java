package repo;


import model.Author;

public interface AuthorRepository {

    void save(Author author);

    Author find(Long authorId);
}
