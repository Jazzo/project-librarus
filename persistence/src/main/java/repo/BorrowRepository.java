package repo;


import model.Borrow;

public interface BorrowRepository {

    void save(Borrow borrow);

    Borrow find(Long borrowId);

    void delete(Borrow borrow);
}
