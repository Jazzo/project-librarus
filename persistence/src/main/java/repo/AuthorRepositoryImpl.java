package repo;

import model.Author;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import util.HibernateUtil;

public class AuthorRepositoryImpl implements AuthorRepository {
    @Override
    public void save(Author author) {
        Transaction transaction = null;
        try (Session session = HibernateUtil.openSession()){
            transaction = session.getTransaction();
            transaction.begin();
            session.persist(author);
            transaction.commit();
        }catch (Exception e){
            if (transaction!=null){
                transaction.rollback();
            }
        }
    }

    @Override
    public Author find(Long authorId) {
        Transaction transaction = null;
        Author author = null;
        try (Session session = HibernateUtil.openSession()) {
            transaction = session.getTransaction();
            transaction.begin();
            Query query = session.createQuery("select a from Author  where a.id = :id")
                    .setParameter("id", authorId);
            author = (Author) query.uniqueResult();
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
        }
        return author;
    }
}
