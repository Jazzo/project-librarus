package repo;



import model.Book;

import java.io.Serializable;
import java.util.List;

public interface BookRepository {

    void save (Book book);

    List<Book> findAll();

    Book find(Long bookId);

    void edit(Book book);

    void delete(Serializable bookId);
}
