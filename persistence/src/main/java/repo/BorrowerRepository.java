package repo;

import model.Borrower;

import java.util.List;

public interface BorrowerRepository {

    List<Borrower> findAll();

    Borrower find(Long borrowerId);
}
