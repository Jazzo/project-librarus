package repo;

import model.Book;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.Transaction;
import util.HibernateUtil;

import java.io.Serializable;
import java.util.List;

public class BookRepositoryImpl implements BookRepository {
    @Override
    public void save(Book book) {
        Transaction transaction = null;
        try(Session session = HibernateUtil.openSession()){
            transaction=session.getTransaction();
            transaction.begin();
            session.persist(book);
            transaction.commit();
        }catch (Exception e){
            if (transaction!=null){
                transaction.rollback();
            }
        }
    }

    @Override
    public List<Book> findAll() {
        return null;
    }

    @Override
    public Book find(Long bookId) {
        return null;
    }

    @Override
    public void edit(Book book) {

    }

    @Override
    public void delete(Serializable bookId) {

    }
}
